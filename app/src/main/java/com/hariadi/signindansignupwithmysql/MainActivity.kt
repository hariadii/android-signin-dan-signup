package com.hariadi.signindansignupwithmysql

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.hariadi.signindansignupwithmysql.activity.MasukActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        Handler().postDelayed({
            startActivity(Intent(this, MasukActivity::class.java))
        }, 1500)
    }
}
