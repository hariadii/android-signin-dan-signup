package com.hariadi.signindansignupwithmysql.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.hariadi.signindansignupwithmysql.R
import kotlinx.android.synthetic.main.activity_buat_akun.*

class BuatAkunActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buat_akun)

        supportActionBar?.hide()

        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
            return@setNavigationOnClickListener
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
